package com.raybritton.swissscalpel;

import android.app.Dialog;
import android.widget.CompoundButton;
import android.widget.Switch;

/**
 * Created by raybritton on 16/04/2016.
 */
public class ControlsDialog extends Dialog {

    private SwissScalpel swissScalpel;
    private Switch idsSwitch;
    private Switch classesSwitch;
    private Switch paddingSwitch;
    private Switch sizesSwitch;

    public ControlsDialog(final SwissScalpel scalpel) {
        super(scalpel.getContext(), R.style.DialogTheme);
        setContentView(R.layout.controls);
        swissScalpel = scalpel;
        idsSwitch = (Switch) findViewById(R.id.ids);
        classesSwitch = (Switch) findViewById(R.id.classes);
        paddingSwitch = (Switch) findViewById(R.id.padding);
        sizesSwitch = (Switch) findViewById(R.id.sizes);

        idsSwitch.setChecked(swissScalpel.isDrawingIds());
        classesSwitch.setChecked(swissScalpel.isDrawingClasses());
        paddingSwitch.setChecked(swissScalpel.isDrawingPadding());
        sizesSwitch.setChecked(swissScalpel.isDrawingSize());

        idsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                scalpel.setDrawIds(isChecked);
            }
        });

        classesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                scalpel.setDrawClasses(isChecked);
            }
        });

        paddingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                scalpel.setDrawPadding(isChecked);
            }
        });

        sizesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                scalpel.setDrawSize(isChecked);
            }
        });
    }
}
