# README #

A copy of Scalpel by Jake Wharton with extra features. See https://github.com/JakeWharton/scalpel for more info

Include 

```
#!groovy

dependencies {
   compile 'com.raybritton.swissscalpel:sscalpel:1.0.2'
}
```


There are two few ways to control scalpel:
1) the original way by calling setters on the view directly:


```
#!android

swissScalpel.setLayerInteractionEnabled();
swissScalpel.setDrawIds(true);
```


or you can use show a dialog with all the controls in it by

2) passing keyevents to the view to automatically show and hide both the view and dialog
(put this in all activities that use the view)

In this mode, pressing volume up will start the scalpel mode, pressing it again will show the control dialog and pressing volume down stops the mode.

```
#!android


@Override
    public boolean dispatchKeyEvent(final KeyEvent event) {
        if(!swissScalpel.handleKeyEvent(event)){
            super.dispatchKeyEvent(event);
        }
        return true;
    }
```


You can also call a function to show the controls at any time (this only shows the control dialog and doesn't start the views main function)


```
#!android

swissScalpel.showControls();
```