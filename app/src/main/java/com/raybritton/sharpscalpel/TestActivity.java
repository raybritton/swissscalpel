package com.raybritton.sharpscalpel;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.raybritton.swissscalpel.SwissScalpel;

public class TestActivity extends AppCompatActivity {

	private SwissScalpel swissScalpel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		swissScalpel = (SwissScalpel) findViewById(R.id.root);
	}

	@Override
	public boolean dispatchKeyEvent(final KeyEvent event) {
		if(!swissScalpel.handleKeyEvent(event)){
			return super.dispatchKeyEvent(event);
		}
		return true;
	}
}
